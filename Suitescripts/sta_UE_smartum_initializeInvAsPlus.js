/**
 * Module Description
 * 
 * Initializes field on the record
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Jun 2020     WLim             Initial Version
 *
 */
 
/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope Public
 */

define(function(require){


    //load modules
    var file      = require('N/file');
    var task      = require('N/task');
    var search    = require('N/search');
    var render    = require('N/render');
    var record    = require('N/record');
    var format    = require('N/format');
    var runtime   = require('N/runtime');
    
    
    /**
     * Function definition to be triggered before record is submitted.
     *
     * @param {Object} context
     * @param {Record} context.newRecord - New record
     * @param {Record} context.oldRecord - Old record
     * @param {string} context.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit_initialize(context) {
        var LOG_TITLE = 'beforeSubmit_initialize';
        
        try
        {
            var contextType = runtime.ContextType;
            log.debug(LOG_TITLE, '- START -' + ' Context : ' + runtime.executionContext + ' ID : ' + context.newRecord.id);
            
            
            //Type check
            if (context.type != context.UserEventType.CREATE && context.type != context.UserEventType.COPY )
            {
                log.debug(LOG_TITLE,'Invalid event type ' + context.type);
                return;
            }
            
            var recCurrent = context.newRecord;

            recCurrent.setValue('custbody_sta_plus_invoice_inspected', true);
            
            
            log.debug(LOG_TITLE, ' - END - ');
        }
        catch (error) 
        {
            //Don't throw error, just catch and log
            if (error.getDetails != undefined) 
            {
                log.error('Process Error', 'Q' + ': ' + error.getCode() + ': ' + error.getDetails());
            }
            else 
            {
                log.error('Unexpected Error', 'Q' + ': ' + error.toString());
            }
        }
    }
    

    function getAccountingPeriodFromDate(stDate)
    {
        stDate = format.format({value: stDate, type: format.Type.DATE});
        var accountingperiodSearchObj = search.create({
           type: "accountingperiod",
           filters:
           [
              ["startdate","onorbefore",stDate], 
              "AND", 
              ["enddate","onorafter",stDate], 
              "AND", 
              ["isquarter","is","F"], 
              "AND", 
              ["isyear","is","F"]
           ],
           columns:
           [
              search.createColumn({
                 name: "periodname",
                 sort: search.Sort.ASC
              }),
              "internalid"
           ]
        });
        var stPeriod = '';
        var searchResultCount = accountingperiodSearchObj.runPaged().count;
        log.debug("accountingperiodSearchObj result count",searchResultCount);
        accountingperiodSearchObj.run().each(function(result){
           // .run().each has a limit of 4,000 results
           stPeriod = result.getValue('internalid');
           return true;
        });
        
        return stPeriod;
    }

    function forceParseFloat(stValue) {
        var flValue = parseFloat(stValue);

        if (isNaN(flValue) || (Infinity == stValue)) {
            return 0.00;
        }

        return flValue;
    }

    /* Function name:  isEmpty
     * Description:    Return true if input is either blank, null, or undefined.
     *                 Return false for any other value, including zero.
     */
    function isEmpty(stValue, zeroIsEmpty) {
        if (stValue == '0') {
            /* If the value is zero, this means that the field is not empty. */
            if (zeroIsEmpty) {
                return true;
            } else {
                return false;
            }
        } else if ((stValue == '') || (stValue == null) || (stValue == undefined)) {
            return true;
        }
    }
    
    function getAllSearchResults(objSearch){
        var arrReturnSearchResults = [];
        var maxResults = 1000;
        var objResultset = objSearch.run();
        var intSearchIndex = 0;
        var arrResultSlice = null;

        do
        {
            arrResultSlice = objResultset.getRange(intSearchIndex, intSearchIndex + maxResults);
            if (arrResultSlice == null)
            {
                break;
            }
            arrReturnSearchResults = arrReturnSearchResults.concat(arrResultSlice);
            intSearchIndex = arrReturnSearchResults.length;
        } 
        while(arrResultSlice.length >= maxResults);

        return arrReturnSearchResults;
    };
    
    function replaceAll(find, replace, str) {
        return str.replace(new RegExp(find, 'g'), replace);
    }
    
    return {
        //beforeLoad:beforeLoad,
        beforeSubmit: beforeSubmit_initialize
    };



});