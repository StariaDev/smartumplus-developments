/**
 *
 * Version    Date            Author           Remarks
 * 1.00       25 Sep 2020     WLim
 * 1.10       09 Oct 2020     WLim             Removed updating functionality, instead skip invoice
 *
 * @NApiVersion 2.x
 * @NScriptType restlet
 * @NModuleScope SameAccount
 */

/**
 * @NApiVersion 2.x
 * @NScriptType restlet
 */
define([
    'N/record',
    'N/search',
    'N/log',
    'N/format',
    'N/search',
    'N/error',
    '/SuiteScripts/moment-with-locales'
],

function(record, search, log, format, search, error, moment)
{
var RESTlet = {};
/**
 * This script handles inbound data to process into Invoices
 * 
 * @author  Wesley Lim
 * @param   objRequestBody - Request from Smartumplus
 * @returns objResponseBody
 */
RESTlet.post = function(objRequestBody)
{
    var LOG_TITLE = 'Restlet.post';
    log.debug(LOG_TITLE, '- Start -');
    
    var objResponseBody = {};
    objResponseBody.successrecords = {};
    objResponseBody.errormessage = {};
    
    var stErrMsgTemplate ='';

    try
    {
        log.debug(LOG_TITLE, 'Payload : ' + JSON.stringify(objRequestBody));
        
        var objData = parseData(objRequestBody);
        //log.debug(LOG_TITLE, 'Parsed Value : ' + objData.name);
        var arrCreatedIDs = [];

        //objResponseBody.errormessage.text = 'no errors encountered';
        objResponseBody.code = '200';
        objResponseBody.successrecords.text = '';
        
        var stCount    = 1;
        var stUpdated  = 0;
        var stImported = 0;

        var recID = objData.last_id;
        var MAX_COUNT = objData.max_result_count;
        log.debug(LOG_TITLE, 'Obtained Record ID : ' + recID + ' max count : ' + MAX_COUNT);
        stErrMsgTemplate = recID + ' ' + ':Error Occured';
        
        if(isEmpty(MAX_COUNT))
        {
            var err = error.create({name: 'NO_MAX_COUNT_DEFINED', message: 'Error while parsing JSON, no MAX COUNT defined'})
            //err.toString = function(){return err.message};
            throw err;
        }

            //Search for existing ID 
            var customerdepositSearchObj = search.create({
                type: "customerdeposit",
                filters:
                [
                   ["internalidnumber","greaterthan",recID], 
                   "AND", 
                   ["type","anyof","CustDep"], 
                   "AND", 
                   ["mainline","is","T"]
                ],
                columns:
                [
                    search.createColumn({
                        name: "internalid",
                        sort: search.Sort.ASC
                     })
                ]
             });
             var searchResultCount = customerdepositSearchObj.runPaged().count;
             log.debug("customerdepositSearchObj result count",searchResultCount);
             var currentCount = 0;
             objResponseBody.deposits = {};
             arrDeposits = [];
             customerdepositSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results

                //Exit loop if max count is reached
                if(currentCount >= MAX_COUNT)
                {
                    return false;
                }
                var objDeposit = {};
                objDeposit.id = result.getValue('internalid');

                arrDeposits.push(objDeposit);
                currentCount++;
                return true;
             });
             objResponseBody.deposits = arrDeposits;


    } 
    catch (objErr)
    {
        objResponseBody.code = '500';
        objResponseBody.errormessage.text = 'Unexpected error occurred during import';
        objResponseBody.errormessage.type = null;
        objResponseBody.errormessage.id = null;
        objResponseBody.successrecords.text = 'Error Occured during import';

        var mainErr = '';
        if (objErr.message != undefined)
        {
            log.error('Process Error', stErrMsgTemplate + ' ' + objErr.name + ' : ' + objErr.message);
            objResponseBody.errormessage.text = objErr.message;
            mainErr = error.create({name: objErr.name, message: stErrMsgTemplate + objErr.message});
        } else
        {
            log.error('Unexpected Error', stErrMsgTemplate + ' ' + objErr.toString());
            objResponseBody.errormessage.text = objErr.message;
            mainErr = error.create({name: 'Unexpected Error', message: stErrMsgTemplate + objErr.toString()});
        }
        
        //var err = error.create({name: objResponseBody.code, message: error.message})
        //err.toString = function(){return err.message};
        
        //throw error;
        
        //err.toString = function(){return err.message};
        throw mainErr;
    }
    log.debug(LOG_TITLE, '- End -');
    return objResponseBody;
    
}

return RESTlet;



/* Function name:  isEmpty
 * Description:    Return true if input is either blank, null, or undefined.
 *                 Return false for any other value, including zero.
 */
function isEmpty(stValue, zeroIsEmpty) {
    if (stValue == '0') {
        /* If the value is zero, this means that the field is not empty. */
        if (zeroIsEmpty) {
            return true;
        }
        else {
            return false;
        }
    }
    else if ((stValue == '') || (stValue == null) || (stValue == undefined)) {
        return true;
    }
}

function parseData(data) {
    if (!data) return {};
    if (typeof data === 'object') return data;
    if (typeof data === 'string') return JSON.parse(data);

    return {};
}
    
function lookUpCountry(id,text,shortCode,returnType)
{
    var countryArr = [ {"value":"0","text":"Afghanistan","short":"AF"},
                    {"value":"1","text":"Aland Islands","short":"AX"},
                    {"value":"2","text":"Albania","short":"AL"},
                    {"value":"3","text":"Algeria","short":"DZ"},
                    {"value":"4","text":"American Samoa","short":"AS"},
                    {"value":"5","text":"Andorra","short":"AD"},
                    {"value":"6","text":"Angola","short":"AO"},
                    {"value":"7","text":"Anguilla","short":"AI"},
                    {"value":"8","text":"Antarctica","short":"AQ"},
                    {"value":"9","text":"Antigua and Barbuda","short":"AG"},
                    {"value":"10","text":"Argentina","short":"AR"},
                    {"value":"11","text":"Armenia","short":"AM"},
                    {"value":"12","text":"Aruba","short":"AW"},
                    {"value":"13","text":"Australia","short":"AU"},
                    {"value":"14","text":"Austria","short":"AT"},
                    {"value":"15","text":"Azerbaijan","short":"AZ"},
                    {"value":"16","text":"Bahamas","short":"BS"},
                    {"value":"17","text":"Bahrain","short":"BH"},
                    {"value":"18","text":"Bangladesh","short":"BD"},
                    {"value":"19","text":"Barbados","short":"BB"},
                    {"value":"20","text":"Belarus","short":"BY"},
                    {"value":"21","text":"Belgium","short":"BE"},
                    {"value":"22","text":"Belize","short":"BZ"},
                    {"value":"23","text":"Benin","short":"BJ"},
                    {"value":"24","text":"Bermuda","short":"BM"},
                    {"value":"25","text":"Bhutan","short":"BT"},
                    {"value":"26","text":"Bolivia","short":"BO"},
                    {"value":"27","text":"Bonaire, Saint Eustatius and Saba","short":"BQ"},
                    {"value":"28","text":"Bosnia and Herzegovina","short":"BA"},
                    {"value":"29","text":"Botswana","short":"BW"},
                    {"value":"30","text":"Bouvet Island","short":"BV"},
                    {"value":"31","text":"Brazil","short":"BR"},
                    {"value":"32","text":"British Indian Ocean Territory","short":"IO"},
                    {"value":"33","text":"Brunei Darussalam","short":"BN"},
                    {"value":"34","text":"Bulgaria","short":"BG"},
                    {"value":"35","text":"Burkina Faso","short":"BF"},
                    {"value":"36","text":"Burundi","short":"BI"},
                    {"value":"37","text":"Cambodia","short":"KH"},
                    {"value":"38","text":"Cameroon","short":"CM"},
                    {"value":"39","text":"Canada","short":"CA"},
                    {"value":"40","text":"Canary Islands","short":"IC"},
                    {"value":"41","text":"Cape Verde","short":"CV"},
                    {"value":"42","text":"Cayman Islands","short":"KY"},
                    {"value":"43","text":"Central African Republic","short":"CF"},
                    {"value":"44","text":"Ceuta and Melilla","short":"EA"},
                    {"value":"45","text":"Chad","short":"TD"},
                    {"value":"46","text":"Chile","short":"CL"},
                    {"value":"47","text":"China","short":"CN"},
                    {"value":"48","text":"Christmas Island","short":"CX"},
                    {"value":"49","text":"Cocos (Keeling) Islands","short":"CC"},
                    {"value":"50","text":"Colombia","short":"CO"},
                    {"value":"51","text":"Comoros","short":"KM"},
                    {"value":"52","text":"Congo, Democratic Republic of","short":"CD"},
                    {"value":"53","text":"Congo, Republic of","short":"CG"},
                    {"value":"54","text":"Cook Islands","short":"CK"},
                    {"value":"55","text":"Costa Rica","short":"CR"},
                    {"value":"56","text":"Cote d'Ivoire","short":"CI"},
                    {"value":"57","text":"Croatia/Hrvatska","short":"HR"},
                    {"value":"58","text":"Cuba","short":"CU"},
                    {"value":"59","text":"Curaçao","short":"CW"},
                    {"value":"60","text":"Cyprus","short":"CY"},
                    {"value":"61","text":"Czech Republic","short":"CZ"},
                    {"value":"62","text":"Denmark","short":"DK"},
                    {"value":"63","text":"Djibouti","short":"DJ"},
                    {"value":"64","text":"Dominica","short":"DM"},
                    {"value":"65","text":"Dominican Republic","short":"DO"},
                    {"value":"66","text":"East Timor","short":"TP"},
                    {"value":"67","text":"Ecuador","short":"EC"},
                    {"value":"68","text":"Egypt","short":"EG"},
                    {"value":"69","text":"El Salvador","short":"SV"},
                    {"value":"70","text":"Equatorial Guinea","short":"GQ"},
                    {"value":"71","text":"Eritrea","short":"ER"},
                    {"value":"72","text":"Estonia","short":"EE"},
                    {"value":"73","text":"Ethiopia","short":"ET"},
                    {"value":"74","text":"Falkland Islands","short":"FK"},
                    {"value":"75","text":"Faroe Islands","short":"FO"},
                    {"value":"76","text":"Fiji","short":"FJ"},
                    {"value":"77","text":"Finland","short":"FI"},
                    {"value":"78","text":"France","short":"FR"},
                    {"value":"79","text":"French Guiana","short":"GF"},
                    {"value":"80","text":"French Polynesia","short":"PF"},
                    {"value":"81","text":"French Southern Territories","short":"TF"},
                    {"value":"82","text":"Gabon","short":"GA"},
                    {"value":"83","text":"Gambia","short":"GM"},
                    {"value":"84","text":"Georgia","short":"GE"},
                    {"value":"85","text":"Germany","short":"DE"},
                    {"value":"86","text":"Ghana","short":"GH"},
                    {"value":"87","text":"Gibraltar","short":"GI"},
                    {"value":"88","text":"Greece","short":"GR"},
                    {"value":"89","text":"Greenland","short":"GL"},
                    {"value":"90","text":"Grenada","short":"GD"},
                    {"value":"91","text":"Guadeloupe","short":"GP"},
                    {"value":"92","text":"Guam","short":"GU"},
                    {"value":"93","text":"Guatemala","short":"GT"},
                    {"value":"94","text":"Guernsey","short":"GG"},
                    {"value":"95","text":"Guinea","short":"GN"},
                    {"value":"96","text":"Guinea-Bissau","short":"GW"},
                    {"value":"97","text":"Guyana","short":"GY"},
                    {"value":"98","text":"Haiti","short":"HT"},
                    {"value":"99","text":"Heard and McDonald Islands","short":"HM"},
                    {"value":"100","text":"Holy See (City Vatican State)","short":"VA"},
                    {"value":"101","text":"Honduras","short":"HN"},
                    {"value":"102","text":"Hong Kong","short":"HK"},
                    {"value":"103","text":"Hungary","short":"HU"},
                    {"value":"104","text":"Iceland","short":"IS"},
                    {"value":"105","text":"India","short":"IN"},
                    {"value":"106","text":"Indonesia","short":"ID"},
                    {"value":"107","text":"Iran (Islamic Republic of)","short":"IR"},
                    {"value":"108","text":"Iraq","short":"IQ"},
                    {"value":"109","text":"Ireland","short":"IE"},
                    {"value":"110","text":"Isle of Man","short":"IM"},
                    {"value":"111","text":"Israel","short":"IL"},
                    {"value":"112","text":"Italy","short":"IT"},
                    {"value":"113","text":"Jamaica","short":"JM"},
                    {"value":"114","text":"Japan","short":"JP"},
                    {"value":"115","text":"Jersey","short":"JE"},
                    {"value":"116","text":"Jordan","short":"JO"},
                    {"value":"117","text":"Kazakhstan","short":"KZ"},
                    {"value":"118","text":"Kenya","short":"KE"},
                    {"value":"119","text":"Kiribati","short":"KI"},
                    {"value":"120","text":"Korea, Democratic People's Republic","short":"KP"},
                    {"value":"121","text":"Korea, Republic of","short":"KR"},
                    {"value":"122","text":"Kosovo","short":"XK"},
                    {"value":"123","text":"Kuwait","short":"KW"},
                    {"value":"124","text":"Kyrgyzstan","short":"KG"},
                    {"value":"125","text":"Lao People's Democratic Republic","short":"LA"},
                    {"value":"126","text":"Latvia","short":"LV"},
                    {"value":"127","text":"Lebanon","short":"LB"},
                    {"value":"128","text":"Lesotho","short":"LS"},
                    {"value":"129","text":"Liberia","short":"LR"},
                    {"value":"130","text":"Libya","short":"LY"},
                    {"value":"131","text":"Liechtenstein","short":"LI"},
                    {"value":"132","text":"Lithuania","short":"LT"},
                    {"value":"133","text":"Luxembourg","short":"LU"},
                    {"value":"134","text":"Macau","short":"MO"},
                    {"value":"135","text":"Macedonia","short":"MK"},
                    {"value":"136","text":"Madagascar","short":"MG"},
                    {"value":"137","text":"Malawi","short":"MW"},
                    {"value":"138","text":"Malaysia","short":"MY"},
                    {"value":"139","text":"Maldives","short":"MV"},
                    {"value":"140","text":"Mali","short":"ML"},
                    {"value":"141","text":"Malta","short":"MT"},
                    {"value":"142","text":"Marshall Islands","short":"MH"},
                    {"value":"143","text":"Martinique","short":"MQ"},
                    {"value":"144","text":"Mauritania","short":"MR"},
                    {"value":"145","text":"Mauritius","short":"MU"},
                    {"value":"146","text":"Mayotte","short":"YT"},
                    {"value":"147","text":"Mexico","short":"MX"},
                    {"value":"148","text":"Micronesia, Federal State of","short":"FM"},
                    {"value":"149","text":"Moldova, Republic of","short":"MD"},
                    {"value":"150","text":"Monaco","short":"MC"},
                    {"value":"151","text":"Mongolia","short":"MN"},
                    {"value":"152","text":"Montenegro","short":"ME"},
                    {"value":"153","text":"Montserrat","short":"MS"},
                    {"value":"154","text":"Morocco","short":"MA"},
                    {"value":"155","text":"Mozambique","short":"MZ"},
                    {"value":"156","text":"Myanmar (Burma)","short":"MM"},
                    {"value":"157","text":"Namibia","short":"NA"},
                    {"value":"158","text":"Nauru","short":"NR"},
                    {"value":"159","text":"Nepal","short":"NP"},
                    {"value":"160","text":"Netherlands","short":"NL"},
                    {"value":"161","text":"Netherlands Antilles (Deprecated)","short":"AN"},
                    {"value":"162","text":"New Caledonia","short":"NC"},
                    {"value":"163","text":"New Zealand","short":"NZ"},
                    {"value":"164","text":"Nicaragua","short":"NI"},
                    {"value":"165","text":"Niger","short":"NE"},
                    {"value":"166","text":"Nigeria","short":"NG"},
                    {"value":"167","text":"Niue","short":"NU"},
                    {"value":"168","text":"Norfolk Island","short":"NF"},
                    {"value":"169","text":"Northern Mariana Islands","short":"MP"},
                    {"value":"170","text":"Norway","short":"NO"},
                    {"value":"171","text":"Oman","short":"OM"},
                    {"value":"172","text":"Pakistan","short":"PK"},
                    {"value":"173","text":"Palau","short":"PW"},
                    {"value":"174","text":"Palestinian Territories","short":"PS"},
                    {"value":"175","text":"Panama","short":"PA"},
                    {"value":"176","text":"Papua New Guinea","short":"PG"},
                    {"value":"177","text":"Paraguay","short":"PY"},
                    {"value":"178","text":"Peru","short":"PE"},
                    {"value":"179","text":"Philippines","short":"PH"},
                    {"value":"180","text":"Pitcairn Island","short":"PN"},
                    {"value":"181","text":"Poland","short":"PL"},
                    {"value":"182","text":"Portugal","short":"PT"},
                    {"value":"183","text":"Puerto Rico","short":"PR"},
                    {"value":"184","text":"Qatar","short":"QA"},
                    {"value":"185","text":"Reunion Island","short":"RE"},
                    {"value":"186","text":"Romania","short":"RO"},
                    {"value":"187","text":"Russian Federation","short":"RU"},
                    {"value":"188","text":"Rwanda","short":"RW"},
                    {"value":"189","text":"Saint Barthélemy","short":"BL"},
                    {"value":"190","text":"Saint Helena","short":"SH"},
                    {"value":"191","text":"Saint Kitts and Nevis","short":"KN"},
                    {"value":"192","text":"Saint Lucia","short":"LC"},
                    {"value":"193","text":"Saint Martin","short":"MF"},
                    {"value":"194","text":"Saint Vincent and the Grenadines","short":"VC"},
                    {"value":"195","text":"Samoa","short":"WS"},
                    {"value":"196","text":"San Marino","short":"SM"},
                    {"value":"197","text":"Sao Tome and Principe","short":"ST"},
                    {"value":"198","text":"Saudi Arabia","short":"SA"},
                    {"value":"199","text":"Senegal","short":"SN"},
                    {"value":"200","text":"Serbia","short":"RS"},
                    {"value":"201","text":"Serbia and Montenegro (Deprecated)","short":"CS"},
                    {"value":"202","text":"Seychelles","short":"SC"},
                    {"value":"203","text":"Sierra Leone","short":"SL"},
                    {"value":"204","text":"Singapore","short":"SG"},
                    {"value":"205","text":"Sint Maarten","short":"SX"},
                    {"value":"206","text":"Slovak Republic","short":"SK"},
                    {"value":"207","text":"Slovenia","short":"SI"},
                    {"value":"208","text":"Solomon Islands","short":"SB"},
                    {"value":"209","text":"Somalia","short":"SO"},
                    {"value":"210","text":"South Africa","short":"ZA"},
                    {"value":"211","text":"South Georgia","short":"GS"},
                    {"value":"212","text":"South Sudan","short":"SS"},
                    {"value":"213","text":"Spain","short":"ES"},
                    {"value":"214","text":"Sri Lanka","short":"LK"},
                    {"value":"215","text":"St. Pierre and Miquelon","short":"PM"},
                    {"value":"216","text":"Sudan","short":"SD"},
                    {"value":"217","text":"Suriname","short":"SR"},
                    {"value":"218","text":"Svalbard and Jan Mayen Islands","short":"SJ"},
                    {"value":"219","text":"Swaziland","short":"SZ"},
                    {"value":"220","text":"Sweden","short":"SE"},
                    {"value":"221","text":"Switzerland","short":"CH"},
                    {"value":"222","text":"Syrian Arab Republic","short":"SY"},
                    {"value":"223","text":"Taiwan","short":"TW"},
                    {"value":"224","text":"Tajikistan","short":"TJ"},
                    {"value":"225","text":"Tanzania","short":"TZ"},
                    {"value":"226","text":"Thailand","short":"TH"},
                    {"value":"227","text":"Togo","short":"TG"},
                    {"value":"228","text":"Tokelau","short":"TK"},
                    {"value":"229","text":"Tonga","short":"TO"},
                    {"value":"230","text":"Trinidad and Tobago","short":"TT"},
                    {"value":"231","text":"Tunisia","short":"TN"},
                    {"value":"232","text":"Turkey","short":"TR"},
                    {"value":"233","text":"Turkmenistan","short":"TM"},
                    {"value":"234","text":"Turks and Caicos Islands","short":"TC"},
                    {"value":"235","text":"Tuvalu","short":"TV"},
                    {"value":"236","text":"Uganda","short":"UG"},
                    {"value":"237","text":"Ukraine","short":"UA"},
                    {"value":"238","text":"United Arab Emirates","short":"AE"},
                    {"value":"239","text":"United Kingdom (GB)","short":"GB"},
                    {"value":"240","text":"United States","short":"US"},
                    {"value":"241","text":"Uruguay","short":"UY"},
                    {"value":"242","text":"US Minor Outlying Islands","short":"UM"},
                    {"value":"243","text":"Uzbekistan","short":"UZ"},
                    {"value":"244","text":"Vanuatu","short":"VU"},
                    {"value":"245","text":"Venezuela","short":"VE"},
                    {"value":"246","text":"Vietnam","short":"VN"},
                    {"value":"247","text":"Virgin Islands (British)","short":"VG"},
                    {"value":"248","text":"Virgin Islands (USA)","short":"VI"},
                    {"value":"249","text":"Wallis and Futuna","short":"WF"},
                    {"value":"250","text":"Western Sahara","short":"EH"},
                    {"value":"251","text":"Yemen","short":"YE"},
                    {"value":"252","text":"Zambia","short":"ZM"},
                    {"value":"253","text":"Zimbabwe","short":"ZW"}];
    var result = '';
    if(id && shortCode == null && text == null)
    {
        for (var key in countryArr)
        {
            if(id.toString() == countryArr[key].value) 
            {
                //nlapiLogExecution('DEBUG','find a state id match');
                if(returnType == 'short')
                    result = countryArr[key].short;
                else if(returnType == 'text')
                    result = countryArr[key].text;
                else
                    result = countryArr[key].value;
            }
        }
    }
    else if (id == null && shortCode && text == null)
    {
        for (var key in countryArr)
        {
            if(shortCode.toString() == countryArr[key].short) 
            {
                //nlapiLogExecution('DEBUG','find a state id match');
                if(returnType == 'short')
                    result = countryArr[key].short;
                else if(returnType == 'text')
                    result = countryArr[key].text;
                else
                    result = countryArr[key].value;
            }
        }
    }
    else if (id == null && shortCode == null && text)
    {
        for (var key in countryArr)
        {
            if(text.toString() == countryArr[key].text) 
            {
                //nlapiLogExecution('DEBUG','find a state id match');
                if(returnType == 'short')
                    result = countryArr[key].short;
                else if(returnType == 'text')
                    result = countryArr[key].text;
                else
                    result = countryArr[key].value;
            }
        }
    }

    log.debug('DEBUG',' look up country result: ' + result);
    return result;				
}

}



);