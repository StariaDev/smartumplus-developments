/**
 * Module Description
 * 
 * Applies a deposit vis transform for an invoice
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 May 2019     WLim             Initial Version
 *
 */
 
 /**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope Public
 */
define(function(require){


    //load modules
    var search        = require('N/search');
    var record        = require('N/record');
    var format        = require('N/format');
    var runtime       = require('N/runtime');
    
    /**
     * Validation function to be executed when sublist line is committed.
     *
     * calculate Discount In Line
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @returns {boolean} Return true if sublist line is valid
     *
     * @since 2015.2
     */
    function afterSubmit_applyDeposits(context) {

        
        try
        {
            var LOG_TITLE = 'applyDeposits';
            var contextType   = runtime.ContextType;
            var stAccountID   = runtime.accountId;
            var currentRecord = context.newRecord;
            currentRecord = record.load({
                type: currentRecord.type,
                id: currentRecord.id,
                isDynamic : false
            });

            log.debug(LOG_TITLE, '- START -' + ' Context : ' + runtime.executionContext + ' ID : ' + context.newRecord.id + ' Account ID : ' + stAccountID);
            
            //Context check, Only Allow UI Context
            //if( runtime.executionContext != contextType.USER_INTERFACE)
            //{
            //    log.error('Context Not Allowed', 'Script is restricted to execute only via UI context only');
            //    return;
            //}
            
            //Type check
            if (context.type != context.UserEventType.CREATE && context.type != context.UserEventType.EDIT )
            {
                log.debug(LOG_TITLE,'Invalid event type ' + context.type);
                return;
            }
            
            
            //Conditions for trigger
            //- Is Smartum+ Invoice (custbody_sta_smartum_plus) is checked
            //- Manual Inspection required  (custbody_sta_plus_inspection_required) = FALSE
            //- 'Plus Deposits are Applied' (custbody_sta_plus_deposit_applied = FALSE
            
            var bIsSmartumPlus = currentRecord.getValue('custbody_sta_smartum_plus');
            var bManualInspReq = currentRecord.getValue('custbody_sta_plus_invoice_inspected');
            var bIsPlusDepAppl = currentRecord.getValue('custbody_sta_plus_deposit_applied');
            
            log.debug(LOG_TITLE,'Trigger Values : ' + 'Is Smartum+ Invoice? '   + bIsSmartumPlus + ' | '
                                                    + 'Manual inspection? '     + bManualInspReq + ' | ' 
                                                    + 'Plus Deposits Applied? ' + bIsPlusDepAppl);
            
            if(!bIsSmartumPlus)
            {
                log.debug(LOG_TITLE, 'Invoice is NOT a Smartum+ Invoice! Exiting.');
                return;
            }
            
            if(!bManualInspReq)
            {
                log.debug(LOG_TITLE, 'Manual Inspection required before proceeding, please review and check the checkbox. Exiting.');
                return;
            }
            
            if(bIsPlusDepAppl)
            {
                log.debug(LOG_TITLE, 'Plus deposits are already applied. Exiting.');
                return;
            }
            
            //Loop through deposits
            var flAmountRemaining = forceParseFloat(currentRecord.getValue('amountremaining'));
            var stCustomer = currentRecord.getValue('entity')
            
            //flAmountRemaining will be obtained via search. (and will deduct amount paid)
            if( flAmountRemaining > 0)
            {
                //do
                //{
                    
                var stMsgEN  = 'A deposit paid in ';
                var stMsgFIN = 'Laskuun on kohdistettu ';
                var lineTracker = 1;
                    
                log.debug(LOG_TITLE, 'Searching for deposits with customer ID  : ' + stCustomer);
                //Search for deposits.
                var customerdepositSearchObj = search.create({
                    type: "customerdeposit",
                    filters:
                    [
                        ["type","anyof","CustDep"], 
                        "AND", 
                        ["status","noneof","CustDep:C","CustDep:R"], 
                        "AND", 
                        ["customer.internalidnumber","equalto",stCustomer], 
                        "AND", 
                        ["mainline","is","T"],
                        "AND", 
                        ["customer.custentity_sta_smartum_plus_customer","is","T"]
                    ],
                    columns:
                    [
                      "internalid",
                      "amount",
                      search.createColumn({
                         name: "trandate",
                         sort: search.Sort.ASC
                      })
                   ]
                });
                var searchResultCount = customerdepositSearchObj.runPaged().count;
                log.debug("customerdeposit Search result count",searchResultCount);
                customerdepositSearchObj.run().each(function(result){
                    // .run().each has a limit of 4,000 results
                   
                    log.debug(LOG_TITLE, 'Processing amount remaining. balance left to be paid : ' + flAmountRemaining);
                   
                    if(flAmountRemaining > 0 )
                    {
                        var stDepositID  = result.getValue('internalid');
                        var stDepDate    = result.getValue('trandate');
                        var flDepositAmt = forceParseFloat(result.getValue('amount'));
                        
                        log.debug(LOG_TITLE, 'Processing Deposit ID : ' + stDepositID + ' with amount : ' + flDepositAmt);
                        
                        //Transform and apply
                        var objDepAppRecord = record.transform({
                            fromType: record.Type.CUSTOMER_DEPOSIT,
                            fromId: stDepositID,
                            toType: record.Type.DEPOSIT_APPLICATION,
                            isDynamic: true,
                        });
                        
                        objDepAppRecord.setValue('trandate', new Date());
                        
                        var intApplyLines = objDepAppRecord.getLineCount({
                            sublistId: 'apply'
                        });

                        log.debug(LOG_TITLE, 'Total Lines: ' + intApplyLines);
                        var applyLine = objDepAppRecord.findSublistLineWithValue({
                            sublistId: 'apply',
                            fieldId: 'internalid',
                            value: currentRecord.id
                        });
                        log.debug(LOG_TITLE, 'Found invoice on line : ' + applyLine);

                        //Make sure Line is found and we have a value to apply
                        if ((applyLine >= 0) && (flDepositAmt > 0))
                        {
                            objDepAppRecord.selectLine({
                                sublistId: 'apply',
                                line: applyLine
                            });
                            
                            objDepAppRecord.setCurrentSublistValue({
                                sublistId: 'apply',
                                fieldId: 'apply',
                                value: true
                            });
                            
                            var stApplyAmount = objDepAppRecord.getCurrentSublistValue({
                                sublistId: 'apply',
                                fieldId: 'amount'
                            });
                            
                            objDepAppRecord.commitLine({
                                sublistId: 'apply'
                            });
                            
                            //Apply and savePreferences
                            var stDepAppId = objDepAppRecord.save({
                                enableSourcing: true,
                                ignoreMandatoryFields: false
                            });
                            log.debug(LOG_TITLE, 'Deposit Application Successfully saved: ' + stDepAppId + '. Applied amount : ' + stApplyAmount);
                            
                            //Set messages
                            if(lineTracker == 1){
                                stMsgEN  = stMsgEN  + stDepDate + ', of ' + stApplyAmount + ' €';
                                stMsgFIN = stMsgFIN + stDepDate + ' suoritettu ennakkomaksu ' + stApplyAmount + ' €';
                            } else
                            {
                                stMsgEN  = stMsgEN  + ' and ' + stDepDate + ', of ' + stApplyAmount + ' €';
                                stMsgFIN = stMsgFIN + ' ja ' + stDepDate + ' suoritettu ennakkomaksu ' + stApplyAmount + ' €';
                            }
                            
                            flAmountRemaining -= flDepositAmt;
                            lineTracker++;
                            //currentRecord.setValue('custbody_sta_plus_deposit_applied', true);
                        }
                        
                        
                        
                    }
                    else
                    {
                        log.debug(LOG_TITLE, 'Invoice Fully paid. Skipping.');
                    }
                    return true;
                });
                    
                    
                //} while (flAmountRemaining > 0);
                
                //Add end of message 
                if(flAmountRemaining <= 0)
                {
                    stMsgEN  = stMsgEN  + ' has been applied to this invoice. Invoice has been fully paid.';
                    stMsgFIN = stMsgFIN + '. Lasku on täysin suoritettu.';
                }
                else
                {
                    stMsgEN  = stMsgEN  + ' has been applied to this invoice. Open amount to be paid is ' + flAmountRemaining + ' €.';
                    stMsgFIN = stMsgFIN + ' ja laskun avoin, maksettava summa on ' + flAmountRemaining + ' €.';
                }
                
                //Add clear text information into invoice field
                var stMsg = '';
                var stLanguage = currentRecord.getValue('custbody_sta_einvoice_language');
                if(stLanguage == 1)
                {
                    //currentRecord.setValue('custbody_sta_text_to_invoice2', stMsgFIN);
                    stMsg = stMsgFIN;
                }else{
                    currentRecord.setValue('custbody_sta_text_to_invoice2', stMsgEN);
                    stMsg = stMsgEN;
                }
                
                //var stInvoiceId = currentRecord.save({
                //    enableSourcing: true,
                //    ignoreMandatoryFields: false
                //});
                
                var stInvoiceId = record.submitFields({
                        type: currentRecord.type,
                        id: currentRecord.id,
                        values: {
                            custbody_sta_plus_deposit_applied: true,
                            custbody_sta_text_to_invoice2: stMsg
                        },
                 options: {
                        enableSourcing: false,
                        ignoreMandatoryFields : true
                     }
                });

                
                log.debug(LOG_TITLE, 'Invoice Successfully saved: ' + stInvoiceId);

            }
            
            
            log.debug(LOG_TITLE, '- END - ');
        }
        catch (error) 
        {
            if (error.getDetails != undefined) 
            {
                log.error('Process Error', 'Q' + ': ' + error.getCode() + ': ' + error.getDetails());
                throw error;
            }
            else 
            {
                log.error('Unexpected Error', 'Q' + ': ' + error.toString());
                throw error;
            }
        }
    }
    
    function forceParseFloat(stValue) {
        var flValue = parseFloat(stValue);

        if (isNaN(flValue) || (Infinity == stValue)) {
            return 0.00;
        }

        return flValue;
    }

    /* Function name:  isEmpty
     * Description:    Return true if input is either blank, null, or undefined.
     *                 Return false for any other value, including zero.
     */
    function isEmpty(stValue, zeroIsEmpty) {
        if (stValue == '0') {
            /* If the value is zero, this means that the field is not empty. */
            if (zeroIsEmpty) {
                return true;
            } else {
                return false;
            }
        } else if ((stValue == '') || (stValue == null) || (stValue == undefined)) {
            return true;
        }
    }
    
    function getAllSearchResults(objSearch){
        var arrReturnSearchResults = [];
        var maxResults = 1000;
        var objResultset = objSearch.run();
        var intSearchIndex = 0;
        var arrResultSlice = null;

        do
        {
            arrResultSlice = objResultset.getRange(intSearchIndex, intSearchIndex + maxResults);
            if (arrResultSlice == null)
            {
                break;
            }
            arrReturnSearchResults = arrReturnSearchResults.concat(arrResultSlice);
            intSearchIndex = arrReturnSearchResults.length;
        } 
        while(arrResultSlice.length >= maxResults);

        return arrReturnSearchResults;
    };
    
    function replaceAll(find, replace, str) {
        return str.replace(new RegExp(find, 'g'), replace);
    }
            
    return {
        //beforeLoad:beforeLoad,
        afterSubmit: afterSubmit_applyDeposits
    };



});